﻿using StudioKit.Data.Entity.Identity.Models;
using StudioKit.Data.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudioKit.Security.Entity.Identity.Models
{
	/// <summary>
	/// A role for an <see cref="IdentityServiceClient"/>. Connects to an <see cref="Role"/>.
	/// </summary>
	public class ServiceClientRole : IAuditable
	{
		[Key, Column(Order = 0)]
		public string ClientId { get; set; }

		[ForeignKey("ClientId")]
		public virtual IdentityServiceClient ServiceClient { get; set; }

		[Key, Column(Order = 2)]
		public string RoleId { get; set; }

		[ForeignKey("RoleId")]
		public virtual Role Role { get; set; }

		[Timestamp]
		public byte[] RowVersion { get; set; }

		public DateTime DateStored { get; set; }

		public DateTime DateLastUpdated { get; set; }

		public string LastUpdatedById { get; set; }
	}
}