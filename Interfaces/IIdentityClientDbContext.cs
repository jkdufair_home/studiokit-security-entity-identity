﻿using StudioKit.Security.Entity.Identity.Models;
using StudioKit.Security.Entity.Interfaces;
using System.Data.Entity;

namespace StudioKit.Security.Entity.Identity.Interfaces
{
	/// <summary>
	/// Interface for a <see cref="DbContext"/> that includes Clients, AppClients, ServiceClients, ServiceClientRoles, and RefreshTokens.
	/// </summary>
	/// <typeparam name="TAppClient">Type of <see cref="AppClient"/></typeparam>
	/// <typeparam name="TServiceClient">Type of <see cref="IdentityServiceClient"/></typeparam>
	/// <typeparam name="TServiceClientRole">Type of <see cref="ServiceClientRole"/></typeparam>
	/// <typeparam name="TRefreshToken">Type of <see cref="RefreshToken"/></typeparam>
	public interface IIdentityClientDbContext<TAppClient, TServiceClient, TServiceClientRole, TRefreshToken>
		: IClientDbContext<TAppClient, TServiceClient, TRefreshToken>
		where TAppClient : AppClient
		where TServiceClient : IdentityServiceClient
		where TServiceClientRole : ServiceClientRole
		where TRefreshToken : RefreshToken
	{
		DbSet<TServiceClientRole> ServiceClientRoles { get; set; }
	}
}