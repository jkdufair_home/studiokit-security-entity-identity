﻿# StudioKit.Security.Entity.Identity

**Note**: Requires the following peer dependencies in the Solution.
* **StudioKit.Security**
* **StudioKit.Security.Entity**

Extends the EntityFramework OAuth models from **StudioKit.Security**. Using **Microsoft.AspNet.Identity.EntityFramework**, defines ServiceClients with specific roles.

## Interfaces

* IIdentityClientDbContext
	* Interface for an IdentityDbContext that includes Clients, AppClients, ServiceClients, ServiceClientRoles, and RefreshTokens.

## Models

* ServiceClientRole
	* A role for an IdentityServiceClient. Connects to an IdentityRole.
* IdentityServiceClient
	* A ServiceClient with specific roles.
